#include "formato.hpp"

void Formato::geraMatriz(){
	for(int i=0; i<12; i++) {
		for(int j=0; j<12; j++) {
			matriz[i][j] = 0;
		}
  }
}

int Formato::getElementoDaMatriz(int linha, int coluna) {
    return matriz[linha][coluna];
}

void Formato::setElementoDaMatriz(int linha, int coluna, int valor) {
    matriz[linha][coluna] = valor;
}
