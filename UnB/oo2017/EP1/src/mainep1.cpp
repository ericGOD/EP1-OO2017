#include <iostream>
#include<stdlib.h>
#include<time.h>
#include"base.hpp"
#include"criadorIntermediario.hpp"
#include"criador.hpp"

using namespace std;

  void sleep(unsigned int mseconds)
  {
      clock_t goal = mseconds + clock();
      while (goal > clock());
  }

  void Inicial(int (*matriz)[80],int cont){
    cout << "geração de numero "<< cont <<endl;
  	for(int i=1;i<=35;i++){
  		for(int j=1;j<=65;j++){
  			if(matriz[i][j] == 0){
          if(j!=1){
            std::cout << "-";
          }
          else if (j==1){
            std::cout << i;
          }
  			}
  			else if(matriz[i][j] == 1){
  				std::cout << "@";
  			}
  		}
  		std::cout << "\n";
  	}
  	sleep(100000);
  	system("clear");
  }

  void Matriz(int (*matriz)[80], int (*aux)[80]){
  	for(int i=0;i<40;i++){
  		for(int j=0;j<80;j++){
  			aux[i][j]=matriz[i][j];
  		}
  	}
  }

  int Vizinhos(int (*matriz)[80], int linha, int coluna){
  	int vizinhos=0;
  	if(matriz[linha-1][coluna-1]==1){
  		vizinhos++;
  	}
  	if(matriz[linha-1][coluna]==1){
  		vizinhos++;
  	}
  	if(matriz[linha-1][coluna+1]==1){
  		vizinhos++;
  	}
  	if(matriz[linha][coluna-1]==1){
  		vizinhos++;
  	}
  	if(matriz[linha][coluna+1]==1){
  		vizinhos++;
  	}
  	if(matriz[linha+1][coluna-1]==1){
  		vizinhos++;
  	}
  	if(matriz[linha+1][coluna]==1){
  		vizinhos++;
  	}
  	if(matriz[linha+1][coluna+1]==1){
  		vizinhos++;
  	}
  	return vizinhos;
  }

  void Interagir(int (*matriz)[80], int (*aux)[80]){
  	for (int i=0;i<40;i++){
  		for(int j=0;j<80;j++){
  			if(matriz[i][j]==1 && Vizinhos(matriz, i, j)>3){
  				aux[i][j]=0;
  			}
  			else if(matriz[i][j]==1 && Vizinhos(matriz, i, j)<2){
  				aux[i][j]=0;
  			}
  			else if(matriz[i][j]==0 && Vizinhos(matriz, i, j)==3){
  				aux[i][j]=1;
  			}
  			else{
  				aux[i][j]=matriz[i][j];
  			}
  		}
  	}
  	Matriz(aux,matriz);
  }

  int main(){
    int contador=0, geracoes;
  	int matriz[40][80]={0};
  	int aux[40][80];
    int resposta;
  	Matriz(matriz,aux);
  	Base primeiro(matriz, 10, 20);
  	CriadorIntermediario segundo(matriz, 8, 30);
  	Criador terceiro(matriz,6, 40);
    Base quarto(matriz, 8, 54);
    std::cout << "###########GunGliderEP1##############" << endl;
    std::cout << "Digite o numero de gerações desejadas."<< endl;
    std::cout << "1- Iniciar" << endl;
    std::cout << "0- Sair" << endl;
  	std::cout << "#####################################"<<endl;
    std::cout << "numero de gerações: ";
    std::cin >> geracoes;
    if(geracoes<=0){
      while(geracoes<=0){
        cout<<"valor inválido, por favor digite novamente!"<<endl;
        std::cout << "numero de gerações: ";
        std::cin >> geracoes;
      }
    }
    std::cout << "iniciar(1/0): ";
    std::cin >>resposta;
    if(resposta !=1 && resposta !=0){
      while(resposta !=1 && resposta !=0){
        cout<<"valor inválido, por favor digite novamente!"<<endl;
        std::cout << "iniciar(1/0): ";
        std::cin >>resposta;
      }
    }
    system("clear");
  	if(resposta==1){
      Inicial(matriz,contador);
    	while(geracoes >= contador){
    	Interagir(matriz, aux);
    	Inicial(matriz,contador);
      contador++;
    	}
    }
  	return 0;
  }
