#include "base.hpp"

Base::Base() {
	setElementoDaMatriz(0, 0, 1);
	setElementoDaMatriz(0, 1, 1);
	setElementoDaMatriz(1, 0, 1);
	setElementoDaMatriz(1, 1, 1);
}
Base::Base(int (*area)[80], int linha, int coluna) {
	geraMatriz();
	setElementoDaMatriz(linha, coluna, 1);
	setElementoDaMatriz(linha, coluna+1, 1);
	setElementoDaMatriz(linha+1, coluna, 1);
	setElementoDaMatriz(linha+1, coluna+1, 1);
	area[linha][coluna]=matriz[linha][coluna];
	area[linha][coluna+1]=matriz[linha][coluna+1];
	area[linha+1][coluna]=matriz[linha+1][coluna];
	area[linha+1][coluna+1]=matriz[linha+1][coluna+1];
}
