#include "criador.hpp"

Criador::Criador() {
	setElementoDaMatriz(2, 0, 1);
	setElementoDaMatriz(3, 0, 1);
	setElementoDaMatriz(4, 0, 1);
	setElementoDaMatriz(2, 1, 1);
	setElementoDaMatriz(3, 1, 1);
	setElementoDaMatriz(4, 1, 1);
	setElementoDaMatriz(1, 2, 1);
	setElementoDaMatriz(5, 2, 1);
	setElementoDaMatriz(0, 4, 1);
	setElementoDaMatriz(1, 4, 1);
	setElementoDaMatriz(5, 4, 1);
	setElementoDaMatriz(6, 4, 1);
}

Criador::Criador(int (*area)[80], int linha, int coluna) {
	geraMatriz();
	setElementoDaMatriz(linha+2, coluna, 1);
	setElementoDaMatriz(linha+3, coluna, 1);
	setElementoDaMatriz(linha+4, coluna, 1);
	setElementoDaMatriz(linha+2, coluna+1, 1);
	setElementoDaMatriz(linha+3, coluna+1, 1);
	setElementoDaMatriz(linha+4, coluna+1, 1);
	setElementoDaMatriz(linha+1, coluna+2, 1);
	setElementoDaMatriz(linha+5, coluna+2, 1);
	setElementoDaMatriz(linha, coluna+4, 1);
	setElementoDaMatriz(linha+1, coluna+4, 1);
	setElementoDaMatriz(linha+5, coluna+4, 1);
	setElementoDaMatriz(linha+6, coluna+4, 1);
	area[linha+2][coluna]=matriz[linha+2][coluna];
	area[linha+3][coluna]=matriz[linha+3][coluna];
	area[linha+4][coluna]=matriz[linha+4][coluna];
	area[linha+2][coluna+1]=matriz[linha+2][coluna+1];
	area[linha+3][coluna+1]=matriz[linha+3][coluna+1];
	area[linha+4][coluna+1]=matriz[linha+4][coluna+1];
	area[linha+1][coluna+2]=matriz[linha+1][coluna+2];
	area[linha+5][coluna+2]=matriz[linha+5][coluna+2];
	area[linha][coluna+4]=matriz[linha][coluna+4];
	area[linha+1][coluna+4]=matriz[linha+1][coluna+4];
	area[linha+5][coluna+4]=matriz[linha+5][coluna+4];
	area[linha+6][coluna+4]=matriz[linha+6][coluna+4];
}
