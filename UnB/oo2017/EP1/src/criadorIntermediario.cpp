#include "criadorIntermediario.hpp"

CriadorIntermediario::CriadorIntermediario() {
	setElementoDaMatriz(2, 0, 1);
	setElementoDaMatriz(3, 0, 1);
	setElementoDaMatriz(4, 0, 1);
	setElementoDaMatriz(1, 1, 1);
	setElementoDaMatriz(5, 1, 1);
	setElementoDaMatriz(0, 2, 1);
	setElementoDaMatriz(6, 2, 1);
	setElementoDaMatriz(0, 3, 1);
	setElementoDaMatriz(6, 3, 1);
	setElementoDaMatriz(3, 4, 1);
	setElementoDaMatriz(1, 5, 1);
	setElementoDaMatriz(5, 5, 1);
	setElementoDaMatriz(2, 6, 1);
	setElementoDaMatriz(3, 6, 1);
	setElementoDaMatriz(4, 6, 1);
	setElementoDaMatriz(3, 7, 1);
}

CriadorIntermediario::CriadorIntermediario(int (*area)[80], int linha, int coluna) {
	geraMatriz();
	setElementoDaMatriz(linha+2, coluna, 1);
	setElementoDaMatriz(linha+3, coluna, 1);
	setElementoDaMatriz(linha+4, coluna, 1);
	setElementoDaMatriz(linha+1, coluna+1, 1);
	setElementoDaMatriz(linha+5, coluna+1, 1);
	setElementoDaMatriz(linha, coluna+2, 1);
	setElementoDaMatriz(linha+6, coluna+2, 1);
	setElementoDaMatriz(linha, coluna+3, 1);
	setElementoDaMatriz(linha+6, coluna+3, 1);
	setElementoDaMatriz(linha+3, coluna+4, 1);
	setElementoDaMatriz(linha+1, coluna+5, 1);
	setElementoDaMatriz(linha+5, coluna+5, 1);
	setElementoDaMatriz(linha+2, coluna+6, 1);
	setElementoDaMatriz(linha+3, coluna+6, 1);
	setElementoDaMatriz(linha+4, coluna+6, 1);
	setElementoDaMatriz(linha+3, coluna+7, 1);
	area[linha+2][coluna]=matriz[linha+2][coluna];
	area[linha+3][coluna]=matriz[linha+3][coluna];
	area[linha+4][coluna]=matriz[linha+4][coluna];
	area[linha+1][coluna+1]=matriz[linha+1][coluna+1];
	area[linha+5][coluna+1]=matriz[linha+5][coluna+1];
	area[linha][coluna+2]=matriz[linha][coluna+2];
	area[linha+6][coluna+2]=matriz[linha+6][coluna+2];
	area[linha][coluna+3]=matriz[linha][coluna+3];
	area[linha+6][coluna+3]=matriz[linha+6][coluna+3];
	area[linha+3][coluna+4]=matriz[linha+3][coluna+4];
	area[linha+1][coluna+5]=matriz[linha+1][coluna+5];
	area[linha+5][coluna+5]=matriz[linha+5][coluna+5];
	area[linha+2][coluna+6]=matriz[linha+2][coluna+6];
	area[linha+3][coluna+6]=matriz[linha+3][coluna+6];
	area[linha+4][coluna+6]=matriz[linha+4][coluna+6];
	area[linha+3][coluna+7]=matriz[linha+3][coluna+7];
}
