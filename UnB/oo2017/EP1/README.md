Um Programa em C++ com classes para objetos printados no console

-Depois de todo o repositório clonado abra a pasta com o comando no console "cd UnB/oo2017/EP1/"
-Após entrar na pasta, no terminal utilize o comando "make"
-Em seguida execute o programa usando o comando "make run" ou "bin/main"

-Com o programa rodando, a tela inicial será bem clara com relação ao conteudo do programa
-Escolha uma geração válida (geração>0), caso não seja inserido um valor válido o software pedirá novamente
-Depois o software perguntará se vc deseja iniciar a Gun Glider ou se deseja sair do software (usando "1" ou "0" como opções), mais uma vez, se for inserido um valor inválido o software repetirá a pergunta.
-E por fim o programa rodará a gun Glider com o numero de gerações desejadas, caso queira iniciar o programa (apertar "1") ou o programa vai encerrar (escolhendo o "0")
 
