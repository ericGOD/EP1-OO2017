#ifndef CRIADOR_HPP
#define CRIADOR_HPP

#include "formato.hpp"

class Criador : public Formato {
	public:
		Criador();
		Criador(int (*area)[80], int linha, int coluna);
};


#endif
