#ifndef FORMATO_HPP
#define FORMATO_HPP

using namespace std;

class Formato{
	public:
		int matriz [12][12];
		void geraMatriz();
		int getElementoDaMatriz(int linha, int coluna);
		void setElementoDaMatriz(int linha, int coluna, int valor);
};

#endif
