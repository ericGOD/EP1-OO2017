#ifndef CRIADORINTERMEDIARIO_HPP
#define CRIADORINTERMEDIARIO_HPP

#include "formato.hpp"

class CriadorIntermediario : public Formato {
	public:
		CriadorIntermediario();
		CriadorIntermediario(int (*area)[80], int linha, int coluna);
};

#endif
