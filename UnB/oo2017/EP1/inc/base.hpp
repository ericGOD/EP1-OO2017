#ifndef BASE_HPP
#define BASE_HPP

#include "formato.hpp"

class Base : public Formato {
	public:
		Base();
		Base(int (*area)[80], int linha, int coluna);
};

#endif
